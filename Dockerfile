# SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

ARG PYTHON_VERSION=3.12

FROM python:${PYTHON_VERSION}

LABEL maintainer="HIFIS <support@hifis.net>"

RUN pip install --upgrade pip \
    && pip install poetry
