# License

Copyright © 2025 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

This work is licensed under multiple licenses:
- The source code is licensed under [MIT](LICENSES/MIT.txt).
- Insignificant files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.3](https://reuse.software/specifications/).
