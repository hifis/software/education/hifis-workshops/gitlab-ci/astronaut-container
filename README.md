<!--
SPDX-FileCopyrightText: 2025 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: CC0-1.0
-->

# Astronaut Container

## Purpose of this Repository

This project will be used to teach workshop participants by example
how building custom container images and pushing them to a container
image registry works in GitLab CI.

## Building the container image from the Dockerfile

Building the container image can be done as follows in the root
directory of your repository:

```bash
docker build --tag astronaut-python:3.12 .
```

## CI Pipeline

The CI pipeline builds custom container images for three Python base
image versions and also pushes them to the container images registry
if the CI pipeline runs on the default branch.

## Contributors

The main contributors of this workshop are:

- Norman Ziegner
- Tobias Huste
- Christian Hueser

## License

Please read the file [LICENSE.md](LICENSE.md) to get information on the
licenses used.
